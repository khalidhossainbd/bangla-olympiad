<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.index');
});

Route::get('/bangla_olympiad_2021', 'MainController@student_validation');
Route::get('/bangla_olympiad_2021/live_room', 'MainController@liveRoom');
Route::post('/bangla_olympiad_2021/live_new_room', 'MainController@liveNewRoom');

Route::post('/uploadTask', 'MainController@arts_upload');

Auth::routes();



Route::get('/school-dashboard', 'HomeController@index')->name('home');
Route::get('/school-profile', 'HomeController@profile');
Route::get('/school-profile/edit', 'HomeController@profileEdit');
Route::patch('/school-profile/update/{id}', 'HomeController@profileUpdate');

Route::get('/change-password', 'HomeController@setup');
Route::patch('/change-password/{id}', 'HomeController@changePassword');

Route::get('/getgroups', 'StudentController@getgroups');
Route::get('/getclass', 'StudentController@getclass');
Route::resource('/students', 'StudentController');

// =================Teaccher Routes=================

Route::get('/teachers/login', 'Teacher\Auth\TeacherLoginController@showTeacherLogin')->name('teacher.login');
Route::post('/teachers/login', 'Teacher\Auth\TeacherLoginController@Login');

Route::get('/teachers/dashboard', 'Teacher\Auth\TeacherController@index');

// ==============Admin Routes =================

Route::get('/admin', 'Admin\Auth\AdminLoginController@showAdminLogin')->name('admin.login');
Route::post('/admin', 'Admin\Auth\AdminLoginController@Login');


Route::get('admin/examiner_list', 'Admin\Teacher\TeacherController@teacherList');
Route::get('admin/add_examiner', 'Admin\Teacher\TeacherController@addTeacher');
Route::post('admin/addteacher', 'Admin\Teacher\TeacherController@createTeacher');
Route::get('admin/teacher/{id}/edit', 'Admin\Teacher\TeacherController@editTeacher');
Route::patch('admin/teacher/{id}', 'Admin\Teacher\TeacherController@updateTeacher');

Route::get('admin/teacher/profile/{id}', 'Admin\Teacher\TeacherController@viewProfile');
Route::get('admin/teacher/profile/edit/{id}', 'Admin\Teacher\TeacherController@editProfile');
Route::patch('admin/teacher/profile/update/{id}', 'Admin\Teacher\TeacherController@updateProfile');


Route::get('/admin/home', 'Admin\Auth\AdminController@index');
Route::get('/admin/user_list', 'Admin\Auth\AdminController@userList');
Route::get('/admin/register', 'Admin\Auth\AdminController@register');
Route::post('/admin/register', 'Admin\Auth\AdminController@registerUser');
Route::get('/admin/user/{id}/edit', 'Admin\Auth\AdminController@editUser');
Route::patch('/admin/user/{id}', 'Admin\Auth\AdminController@updateUser');
Route::get('/admin/user/changepassword', 'Admin\Auth\AdminController@changeUserPassword');
Route::patch('/admin/changepassword/{id}', 'Admin\Auth\AdminController@updatePassword');

Route::get('/admin/school_list', 'Admin\School\SchoolController@schoolList');
Route::get('/admin/school/{id}/edit', 'Admin\School\SchoolController@schoolEdit');
Route::patch('/admin/school/{id}', 'Admin\School\SchoolController@schoolUpdate');

Route::get('/admin/school/profile/{id}', 'Admin\School\SchoolController@schoolProfile');

Route::get('/admin/student_list', 'Admin\StudentController@studentList');

Route::resource('/admin/category', 'Admin\\CategoryController');
Route::resource('/admin/groups', 'Admin\\GroupController');



Route::get('/{any}', function ($any) {

  return Redirect::to('/');

})->where('any', '.*');
