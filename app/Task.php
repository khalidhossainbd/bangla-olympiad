<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

	protected $table = 'tasks';

    protected $primaryKey = 'id';

    
    protected $fillable = [
    	'taskID',
        'student_id',
        'files',
        'category_id',
        'group_id',
        'user_id',
        'code',
        'reward',
        'status'
    ];
}
