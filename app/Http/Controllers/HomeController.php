<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SchoolProfile;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Redirect,Response;

class HomeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('schools.index');
    }

    public function profile()
    {
        $data = SchoolProfile::findOrFail(Auth::user()->id);
        return view('schools.school_profile', compact('data'));
    }

    public function profileEdit()
    {
        $data = SchoolProfile::findOrFail(Auth::user()->id);
        return view('schools.school_profile_edit', compact('data'));
    }

    public function profileUpdate(Request $request, $id)
    {
        // dd($request);
         $file = $request->file('images');

        if (!empty($file)){

            $image = Image::make($file);

            $profile = SchoolProfile::findOrFail($id);

            $path = 'uploads/profiles/';

            $fileName = $request->file('images')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).time().".".$filetype;
          
            $image->save($path.$filenewname);
            
            $image->resize(300, 300);
            $image->save($path.$filenewname);

            $profile->school_phone = $request->school_phone;
            $profile->coordinator = $request->coordinator;
            $profile->mobile_number = $request->mobile_number;
            $profile->school_address = $request->school_address;            
            $profile->images = $filenewname;

            $profile->update();

        }else{

            $profile = SchoolProfile::findOrFail($id);
            $profile->school_phone = $request->school_phone;
            $profile->coordinator = $request->coordinator;
            $profile->mobile_number = $request->mobile_number;
            $profile->school_address = $request->school_address;   
            $profile->update();
        }

        return Redirect::to("/school-profile")->withSuccess('Great! School Profile update successfully.');
    }

    public function setup()
    {
        return view('schools.setup');
    }

    public function changePassword(Request $request, $id)
    {
        // dd($request);
        $data = request()->validate([
                'oldPassword' => 'required',
                'password' => ['required', 'string', 'min:8', 'confirmed'],            
            ]);

            if(Hash::check($request->oldPassword, Auth::user()->password)){
                $user=User::find(Auth::user()->id);
                $user->email=Auth::user()->email;
                $user->password=Hash::make($request->password);
                // dd($user->password);
                $user->update();

                return back()->withSuccess('Great! Password update successfully.');
            }else{
                return back()->withSuccess("Old Password didn't match");
            }
    }

}
