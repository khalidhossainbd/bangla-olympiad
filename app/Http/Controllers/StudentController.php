<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Category;
use App\Group;
use App\Student;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Redirect,Response;

class StudentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::where('user_id', '=', Auth::user()->id)->where('status', '=', '1')->orderBy('created_at', 'desc')->get();
        return view('students.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schools = User::where('status', '=', '1')->get();
        $category = Category::where('status', '=', '1')->get();
        return view('students.create', compact('schools', 'category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
                   'category_id' => 'required',
                   'group_id' => 'required',
                   'name' => 'required',
               ]);

        $val_data = Student::where('category_id', $request->category_id)->where('group_id', $request->group_id)->where('user_id', Auth::user()->id)->get();

        if(count($val_data)>2){
            return Redirect::to("/students")->withSuccess('Students add limited 3!');
        }else{
            $student = new Student();

            $student->studentID = time().rand(1111111,9999999);
            $student->name = $request->name;
            $student->class = $request->class;
            $student->email = $request->email;
            $student->mobile = $request->mobile;
            $student->category_id = $request->category_id;
            $student->group_id = $request->group_id;
            $student->user_id = Auth::user()->id;
            $student->code = rand(111,999)."IH".rand(111,999)."21";

            $student->save();

            return Redirect::to("/students")->withSuccess('Great! Students add successfully.');
        }
              
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::FindOrFail($id);
        $schools = User::where('status', '=', '1')->get();
        $category = Category::where('status', '=', '1')->get();
        return view('students.edit', compact('schools', 'category', 'student'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
                   'category_id' => 'required',
                   'group_id' => 'required',
                   'name' => 'required',
               ]);

        $val_data = Student::where('category_id', $request->category_id)->where('group_id', $request->group_id)->where('user_id', Auth::user()->id)->get();

        if(count($val_data)>3){
            return Redirect::to("/students")->withSuccess('Students add limited 3!');
        }else{
            $student = Student::FindOrFail($id);

            $student->name = $request->name;
            $student->class = $request->class;
            $student->email = $request->email;
            $student->mobile = $request->mobile;
            $student->category_id = $request->category_id;
            $student->group_id = $request->group_id;
            $student->user_id = Auth::user()->id;

            $student->update();

            return Redirect::to("/students")->withSuccess('Great! Students info update successfully.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getgroups(Request $request)
      {
          $data = Group::select('id', 'title', 'class_name')->where('category_id', $request->id)->where('status', '=', '1')->get();
         return response()->json($data); 
      }

      public function getclass(Request $request)
      {
         $data = Group::select('class_name')->where('id', $request->id)->get();
         return response()->json($data); 
      }
}
