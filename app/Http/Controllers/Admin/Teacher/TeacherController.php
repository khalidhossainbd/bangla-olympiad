<?php

namespace App\Http\Controllers\Admin\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Redirect,Response;
use App\Teacher;
use App\TeacherProfile;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;


class TeacherController extends Controller
{
	public function __construct()
	{
	    $this->middleware('auth:admin');
	}
    
    public function teacherList()
    {
        $data = Teacher::all();
        return view('admin.teacher.auth.teacher_list', compact('data'));
    }

    public function addTeacher()
    {
    	return view('admin.teacher.auth.teacheradd');
    }

    public function createTeacher(Request $request)
    {
    	// dd($request);
    	$data = request()->validate([
    	        'name' => ['required', 'string', 'max:255'],
    	        'email' => ['required', 'string', 'email', 'max:255', 'unique:teachers'],
    	        'password' => ['required', 'string', 'min:8'],
    	        'mobile' =>['required', 'min:11', 'unique:teachers'],
    	        'status'=>['required','string'],
    	        'designation'=>['string'],
    	        ]);
    	
    	$check =  Teacher::create([
    	    'teacherID' => time().rand(11111,99999),
    	    'name' => $data['name'],
    	    'url_name' => str_replace(' ', '-', trim($data['name'])),
    	    'email' => $data['email'],
    	    'mobile' => $data['mobile'],
    	    'status'=>$data['status'],
    	    'designation'=>$data['designation'],
    	    'password' => Hash::make($data['password']),
    	]);

        // dd($check->id);
        TeacherProfile::create([
            'teacher_id'=> $check->id,
            'images'=>'dummy.jpg',
        ]);

    	return Redirect::to("admin/examiner_list")->withSuccess('Great! Examiner Create successfully.');
    }

    public function editTeacher($id)
    {
        $data = Teacher::findOrFail($id);
        return view('admin.teacher.auth.teacher_edit', compact('data'));   
    }

    public function updateTeacher(Request $request, $id)
    {
        // dd($request);
        request()->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'mobile' =>['required', 'min:11'],
                'status'=>['required','string'],
                'designation'=>['string'],
                ]);
        $newdata = Teacher::findOrFail($id);

        $newdata->name = $request->name;
        $newdata->url_name =  str_replace(' ', '-', trim($request->name));
        $newdata->email = $request->email;
        $newdata->mobile = $request->mobile;
        $newdata->designation = $request->designation;
        $newdata->status = $request->status;

        $newdata->update();

        return Redirect::to("admin/examiner_list")->withSuccess('Great! Examiner info update successfully.');

    }

    public function viewProfile($id)
    {
        $teacher = Teacher::findOrFail($id);
        $pro_data = TeacherProfile::where('teacher_id', '=', $id)->first();

        return view('admin.teacher.teacherProfile', compact('pro_data', 'teacher'));
    }

    public function editProfile($id)
    {
        $pro_data = TeacherProfile::findOrFail($id);
        return view('admin.teacher.teacherProfileEdit', compact('pro_data'));
    }

    public function updateProfile(Request $request, $id)
    {
        // dd($request);
        $file = $request->file('images');

        if (!empty($file)){

            $image = Image::make($file);

            $profile = TeacherProfile::findOrFail($id);

            $path = 'uploads/profiles/';

            $fileName = $request->file('images')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).time().".".$filetype;
          
            $image->save($path.$filenewname);
            
            $image->resize(300, 300);
            $image->save($path.$filenewname);

            $profile->father_name = $request->father_name;
            $profile->mother_name = $request->mother_name;
            $profile->gender = $request->gender;
            $profile->home_phone = $request->home_phone;
            $profile->date_of_birth = $request->date_of_birth;
            $profile->year_experience = $request->year_experience;
            $profile->content = $request->content;
            $profile->parmanent_address = $request->parmanent_address;
            $profile->preaent_address = $request->preaent_address;
            $profile->images = $filenewname;

            $profile->update();

        }else{

            $profile = TeacherProfile::findOrFail($id);
            $profile->father_name = $request->father_name;
            $profile->mother_name = $request->mother_name;
            $profile->gender = $request->gender;
            $profile->home_phone = $request->home_phone;
            $profile->date_of_birth = $request->date_of_birth;
            $profile->year_experience = $request->year_experience;
            $profile->content = $request->content;
            $profile->parmanent_address = $request->parmanent_address;
            $profile->preaent_address = $request->preaent_address;
            $profile->update();
        }

        return Redirect::to("admin/examiner_list")->withSuccess('Great! Examiner Profile update successfully.');
    }
}
