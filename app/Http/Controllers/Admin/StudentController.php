<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use App\Student;

class StudentController extends Controller
{

	public function __construct()
	{
	    $this->middleware('auth:admin');
	}

	
    public function studentList()
    {
    	$students = Student::all();
    	return view('admin.pages.students.index', compact('students'));
    }
}
