<?php

namespace App\Http\Controllers\Admin\School;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;

class SchoolController extends Controller
{
	public function __construct()
	{
	    $this->middleware('auth:admin');
	}

    public function schoolList()
    {
    	$schools = User::all();
    	return view('admin.pages.schools.index', compact('schools'));
    }

    public function schoolProfile($id)
    {
    	$profile = User::findOrFail($id);
    	return view('admin.pages.schools.profile', compact('profile'));
    }

    public function schoolEdit($id)
    {
    	$school = User::findOrFail($id);
    	return view('admin.pages.schools.edit', compact('school'));
    }

    public function schoolUpdate(Request $request, $id)
    {
    	$requestData = $request->all();
    	$school = User::findOrFail($id);
    	$school->update($requestData);
    	return redirect('/admin/school_list')->with('flash_message', 'School info updated!');
    }
}
