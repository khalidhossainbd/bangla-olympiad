<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;

use App\User;
use App\Category;
use App\Group;
use App\Student;
use App\Task;
use Auth;
use Session;
use Redirect,Response;

class MainController extends Controller
{
    public function student_validation()
    {
    	return view('pages.upload.student_validation');
    }

    public function liveRoom()
    {
    	return view('pages.upload.live_new_room');
    }

    public function liveNewRoom(Request $request)
    {
        $request->validate([
                   'password' => 'required',
               ]);
        // dd($request->password);
        $student = Student::where('code', '=', $request->password)->first();
        $task = Task::where('code', '=', $request->password)->get();
        if(!empty($student)){
            if(count($task)>0){
                return back()->withSuccess('Your Task Already submitted');
            }else{
                if($student->category_id == 5){
                    return view('pages.upload.live_new', compact('student'));
                }else{
                    return view('pages.upload.live_new_room', compact('student'));
                }
            }
            
        }else{
            return back()->withSuccess('Sorry Student Code is not Vaild.');
        }
    	
    }


    public function arts_upload(Request $request)
    {
        // dd($request);
        $request->validate([
                   'student_id' => 'required',
                   'files' => 'required'
               ]);

        $file = $request->file('files');

        // dd($file);

        if (!empty($file)){

            $image = Image::make($file);

            $student = Student::where('studentID', '=', $request->student_id)->first();
            
            $path = 'uploads/tasks/';

            $fileName = $request->file('files')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).time().".".$filetype;
          
             $image->save($path.$filenewname);
            // $request->file->move($path, $fileName);

            $mytask = new Task();
            $mytask->taskID = time().rand(1111111,9999999);
            $mytask->student_id = $student->id;
            $mytask->files = $filenewname;
            $mytask->category_id = $student->category_id;
            $mytask->group_id = $student->group_id ;
            $mytask->user_id = $student->user_id;
            $mytask->code = $student->code;

            $mytask->save();


          return redirect('/bangla_olympiad_2021')->withSuccess('Task submitted successfully.');
        }
    }

}
