<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherProfile extends Model
{
    protected $table = 'teacher_profiles';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

	    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
   	protected $fillable = [
   	    'father_name','mother_name', 'date_of_birth', 'gender', 'year_experience', 'files', 'parmanent_address', 'preaent_address', 'home_phone', 'content', 'images', 'teacher_id'
   	];

   	public function teacher()
    {
        return $this->hasOne(Teacher::class);
    }
}
