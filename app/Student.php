<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
    	'studentID',
        'name',
        'class',
        'email',
        'mobile',
        'reward',
        'status',
        'category_id',
        'group_id',
        'user_id',
        'code',
    ];



    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    public function group()
    {
        return $this->belongsTo('App\Group');
    }
}
