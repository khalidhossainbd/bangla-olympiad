<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Ample Admin Template - The Ultimate Multipurpose admin template</title>
    <link href="{{ asset('teach/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="{{ asset('teach/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{ asset('teach/css/animate.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('teach/css/style.css') }}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{ asset('teach/css/colors/default.css') }}" id="theme" rel="stylesheet">
    
</head>

<body class="fix-header">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <div id="wrapper">
        @include('teacher.partials.mainnavbar')
        @include('teacher.partials.mainsidebar')

        <div id="page-wrapper">
    
        @yield('content')

        @include('teacher.partials.mainfooter')

        </div>
    </div> 


    <script src="{{ asset('teach/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('teach/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('teach/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
    <script src="{{ asset('teach/js/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('teach/js/waves.js') }}"></script>
    <script src="{{ asset('teach/js/custom.min.js') }}"></script>
</body>

</html>
