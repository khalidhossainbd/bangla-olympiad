    <!-- =============Footer section start==================== -->
    <section>
        <div class="footer-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <img class="img-fluid" src="{{ asset('assets/dist/images/logo.png') }}" alt="Logo" style="max-height: 120px">
                        <div class="clearfix"></div>

                        <p class="text-white mb-0 mt-4">Email: info@banglaolympiad.org</p>
                        <p class="text-white m-0">Mobile: +880 1713 492666</p>
                        <div class="social-icon mt-0">
                            <ul>
                                <li><a href="" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <p class="text-white font-weight-bold">INFORMATION & HELP</p>
                        <div class="dropdown-divider"></div>
                        <ul class="list-unstyled">
                            <li><a class="text-white" href="#">About Academy</a></li>
                            <div class="dropdown-divider"></div>
                            <li><a class="text-white" href="#">Admission & Enroll</a></li>
                            <div class="dropdown-divider"></div>
                            <li><a class="text-white" href="#">Fees and Charges</a></li>
                            <div class="dropdown-divider"></div>
                            <li><a class="text-white" href="#">Academic Policy</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-4">
                        
                    </div>
                </div>
            </div>
            <hr class="hr-three">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <p class="text-white text-center text-sm-left">Copyright 2014 - 2020 Bangla Olympiad </p>
                    </div>
                    <div class="col-12 col-sm-6">
                        <p class="text-white text-center text-sm-right">Design & Developed By: 
                            <a class="text-white" href="https://www.linkedin.com/in/khalidhossainbd/" target="_blank">Md Khalid Hossain</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =============Footer section End ===================== -->