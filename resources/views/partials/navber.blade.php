<section>
  <div class="container my-3">
    <div class="row py-1">
      <div class="col-12 col-sm-4">
        <div class="bang-title">
          <h1 class="text-center">বাংলা অলিম্পিয়াড</h1>
            <p style="text-align:center; font-family: 'AdorshoLipi', sans-serif;">ইন্টারনেশনাল হোপ স্কুল বাংলাদেশ এর একটি উদ্যোগ
            </p>
        </div>
      </div>
      <div class="col-12 col-sm-4">
        <img class="img-fluid d-block mx-auto" src="{{ asset('assets/dist/images/logo.png') }}" alt="Logo" style="max-height: 120px;">
      </div>
      <div class="col-12 col-sm-4">
        <div class="eng-title">
          <h1 class="top-title text-center">Bangla Olympiad</h1>
            <p class="text-center">A project of international Hope School Bangladesh</p>
        </div>
      </div>
    </div>
  </div>
</section>

  <nav class="navbar navbar-expand-lg navbar-light bg-primary shadow">
    <div class="container">
    <a class="navbar-brand" href="{{ url('/') }}">Bangla Olympiad</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        {{-- <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li> --}}
      </ul>
      {{-- <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form> --}}
      <ul class="navbar-nav ml-auto">

         @guest
            <li class="nav-item">
                <a class="nav-link btn btn-secondary m-2 my-sm-0 text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link btn btn-secondary m-2 my-sm-0 text-white" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
        {{-- <li class="nav-item">
          <a href="{{ route('login') }}" class="btn btn-outline-success m-2 my-sm-0">Login</a>
        </li>
        
        <li class="nav-item">
          <a href="{{ route('register') }}" class="btn btn-outline-success m-2 my-sm-0">Register</a>
        </li> --}}
      </ul>
    </div>
  </div>
</nav>
