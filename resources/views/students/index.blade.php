@extends('layouts.main')

@section('title', 'School Deshboard')

@section('content')

<div class="bg-light">
	<div class="container mt-4">
		<div class="row">
			<div class="col">
				<h3 class="mt-5"></h3>
			</div>
			
		</div>
	</div>
</div>



  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-new-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading"> <b>School Deshboard</b>   </div>
      <hr>
      <div class="list-group list-group-flush">
        <a href="{{ url('/school-dashboard') }}" class="list-group-item list-group-item-action bg-new-light">Dashboard</a>
        <a href="{{ url('/school-profile') }}" class="list-group-item list-group-item-action bg-new-light">Profile</a>
        <a href="{{ route('students.index') }}" class="list-group-item list-group-item-action bg-new-light">Student List</a>
        <a href="{{ route('students.create') }}" class="list-group-item list-group-item-action bg-new-light">Add Student</a>
        <a href="#" class="list-group-item list-group-item-action bg-new-light">Submitted Task </a>
        <a href="{{ url('/change-password') }}" class="list-group-item list-group-item-action bg-new-light">Change Password</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></button> 
        <h3 class="pl-3">Welcome to Bangla Olympiad 2021</h3>

      </nav>



      <div class="container-fluid">
        @if ($message = Session::get('success'))
        <hr>
           <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
           </div>
           <br>
        <hr>
        @endif
        <div class="row">
          <div class="col-12 p-3">
            <table class="table table-bordered">
              <thead>
                <th>Sl No.</th>
                <th>Student Code</th>
                <th>Student Name</th>
                <th>Student Mobile</th>
                <th>Category Title</th>
                <th>Group Name</th>
                <th>Action</th>
              </thead>
              <tbody>
                @foreach($students as $student)
                <tr>
                  <td>{{ $loop->iteration  }}</td>
                  <td>{{ $student->code }}</td>
                  <td>{{ $student->name }}</td>
                  <td>{{ $student->mobile }}</td>
                  <td>{{ $student->category->title }}</td>
                  <td>{{ $student->group->title }}</td>
                  <td>
                    <a class="btn btn-sm btn-info" href="{{ route('students.edit', $student->id) }}">Edit</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

  </div>




@endsection