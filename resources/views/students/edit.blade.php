@extends('layouts.main')

@section('title', 'School Deshboard')

@section('content')

<div class="bg-light">
	<div class="container mt-4">
		<div class="row">
			<div class="col">
				<h3 class="mt-5"></h3>
			</div>
			
		</div>
	</div>
</div>



  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-new-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading"> <b>School Deshboard</b>   </div>
      <hr>
      <div class="list-group list-group-flush">
        <a href="{{ url('/school-dashboard') }}" class="list-group-item list-group-item-action bg-new-light">Dashboard</a>
        <a href="{{ url('/school-profile') }}" class="list-group-item list-group-item-action bg-new-light">Profile</a>
        <a href="{{ route('students.index') }}" class="list-group-item list-group-item-action bg-new-light">Student List</a>
        <a href="{{ route('students.create') }}" class="list-group-item list-group-item-action bg-new-light">Add Student</a>
        <a href="#" class="list-group-item list-group-item-action bg-new-light">Submitted Task </a>
        <a href="{{ url('/change-password') }}" class="list-group-item list-group-item-action bg-new-light">Change Password</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></button> 
        <h3 class="pl-3">Welcome to Bangla Olympiad 2021</h3>
 
      </nav>

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <h3 class="my-3">Student Info Edit</h3>
          </div>
        </div>
      </div>

      <div class="container-fluid">
        <div class="">
          <form method="post" action="{{ route('students.update', $student->id) }}" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="row">
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <label for="exampleFormControlSelect1">Select Category</label>
                  <select class="form-control demo2" name="category_id">
                    <option value="{{ $student->category_id }}" selected>{{ $student->category->title }}</option>
                    @foreach($category as $cat)
                    <option value="{{ $cat->id }}">{{ $cat->title }}</option>
                    @endforeach
                    
                  </select>
                </div>
                <div class="form-group">
                  <label for="event_title">Group Name</label>
                  <select class="form-control demo3 demo4 " name="group_id" required="required">
                    <option value="{{ $student->group_id }}" selected>{{ $student->group->title }}</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="event_title">Class</label>
                  <select class="form-control demo5 " name="class">
                    <option value="{{ $student->class }}" selected>{{ $student->class }}</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="name">Student Name</label>
                  <input type="text" name="name" class="form-control" id="name" value="{{ $student->name }}" required>
                </div>
                <div class="form-group">
                  <label for="email">Student Email</label>
                  <input type="email" name="email" class="form-control" id="email" value="{{ $student->email }}" required>
                </div>
                <div class="form-group">
                  <label for="email">Student Mobile</label>
                  <input type="text" name="mobile" class="form-control" id="email" value="{{ $student->mobile }}" required>
                </div>
                <button type="submit" class="btn btn-success">Update Participant</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>




@endsection



@section('java_script')
<script>
$(document).ready(function(){
$(document).on('change', '.demo2', function(){
// console.log('It is ok');
var category_id = $(this).val();
var opp= " ";
$.ajax({
type:'get',
url:'{{ url('/getgroups')}}',
data:{ 'id' : category_id},
success: function(data){
opp+= '<option value="0" selected disabled >--select an option-- </option>';
for (var i = 0; i < data.length; i++) {
opp+= '<option value="'+ data[i].id +'">'+ data[i].title + '</option>';
}
$('.demo3').html(opp);
},
error:function(){
}
});
});
});
$(document).ready(function(){
$(document).on('change', '.demo4', function(){
//
var group_id = $(this).val();
var oppn= " ";
$.ajax({
type:'get',
url:'{{ url('/getclass')}}',
data:{ 'id' : group_id},
// console.log('It is ok'),
success: function(data){
var opp = "";
for (var i = 0; i < data.length; i++) {
opp+=  data[i].class_name;
}
var array = opp.split(',');
oppn+= '<option value="0" selected disabled >--select an option-- </option>';
for (var i = 0; i < array.length; i++) {
oppn+= '<option value="'+ array[i] +'">'+ array[i]  + '</option>';
}
$('.demo5').html(oppn);
},
error:function(){
}
});
});
});
</script>
@endsection