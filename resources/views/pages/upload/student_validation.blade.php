@extends('layouts.main')

@section('title', 'Student live room')

@section('content')
<div class="container my-5">

    <div class="row">
        <div class="col-12 col-sm-5 mt-5">
            <img class="img-fluid shadow" src="{{ asset('assets/dist/images/unnamed.jpg') }}" alt="">
        </div>
        <div class="col-12 col-sm-7">
            <div class="card mt-5 shadow bg-light">
                <div class="card-header">Login Here</div>

                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <hr>
                       <div class="alert alert-success alert-block">
                          <button type="button" class="close" data-dismiss="alert">×</button>
                          <strong>{{ $message }}</strong>
                       </div>
                       <br>
                    <hr>
                    @endif
                    <form method="POST" action="{{ url('/bangla_olympiad_2021/live_new_room') }}">
                        @csrf

                        {{-- <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">School Name</label>

                            <div class="col-md-6">
                               <div class="form-group">
                                <select class="form-control" id="exampleFormControlSelect1">
                                  <option>School 1</option>
                                  <option>School 2</option>
                                  <option>School 3</option>
                                  <option>School 4</option>
                                  <option>School 5</option>
                                </select>
                              </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Branch Name</label>
                            <div class="col-md-6">
                               <div class="form-group">
                                <select class="form-control" id="exampleFormControlSelect1">
                                  <option>School 1</option>
                                  <option>School 2</option>
                                  <option>School 3</option>
                                  <option>School 4</option>
                                  <option>School 5</option>
                                </select>
                              </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Event For</label>
                            <div class="col-md-6">
                               <div class="form-group">
                                <select class="form-control" id="exampleFormControlSelect1">
                                  <option>School 1</option>
                                  <option>School 2</option>
                                  <option>School 3</option>
                                  <option>School 4</option>
                                  <option>School 5</option>
                                </select>
                              </div>
                            </div>
                        </div> --}}

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Student Code</label>

                            <div class="col-md-6">
                                <input id="password" type="text" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">Submit & Proceed</button> 
                            </div>
                        </div>

                        {{-- <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <a href="{{ url('/bangla_olympiad_2021/live_new_room') }}" class="btn btn-primary">
                                    Go To Live
                                </a>

                            </div>
                        </div> --}}
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-5 shadow bg-light">
                <div class="card-header">Login Here</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}
</div>
@endsection
