@extends('layouts.main')

@section('title', 'Student live room')


@section('content')
<div class="container my-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-5 shadow bg-light">
                <div class="card-header">Record Your Live Video</div>

                <div class="card-body">
                    <form method="post" action="{{ url('/uploadTask') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="files" class="col-md-4 col-form-label text-md-right">Upload Task</label>
                            <input type="text" class="d-none" name="student_id" value="{{ $student->studentID }}">
                            <div class="col-md-6">
                                <input id="files" type="file" class="form-control-file" name="files" required >

                                @error('files')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">Save Task</button> 
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


