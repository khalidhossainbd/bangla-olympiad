@extends('layouts.main')

@section('title', 'Student live room')


@section('content')
    <div class="container" style="margin-top: 50px;">
      <div class="row justify-content-center d-none">
        <div class="col-8">
          {{-- <div class="time-counter">
            <h1 class="screen text-center">
              <span id="minutes">
                00
              </span>:<span id="seconds">
                00
              </span>
              <span class="d-none" id="hundredths">
                00
              </span>
            </h1>
          </div> --}}
            <div class="buttons">
              {{-- <button id="start">START</button> --}}
              <button id="stop">STOP</button>
              <button id="reset">RESET</button>
            </div>
          
        </div>
        
      </div>
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card my-5 shadow bg-light">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-6">
                        Record Your Live Video
                      </div>
                      <div class="col-6">
                        <div class="time-counter">
                          <h4 class="screen text-right">
                            <span id="minutes">
                              00
                            </span>:<span id="seconds">
                              00
                            </span>
                            <span class="d-none" id="hundredths">
                              00
                            </span>
                          </h4>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="card-body">

                      <div class="embed-responsive embed-responsive-21by9">
                        <video id="gum" playsinline autoplay muted></video>
                      </div>
                      

                      <div class="row mt-3">
                          <div class="col-6">
                              <button class="btn btn-sm btn-block btn-danger" id="start">Start camera</button>
                          </div>
                          <div class="col-6">
                              <button class="btn btn-sm btn-block btn-info" id="record" disabled>Start Recording</button>
                          </div>
                      </div>

                      <div class="embed-responsive embed-responsive-21by9 mt-5">
                       <video id="recorded" playsinline loop></video>
                      </div>

                      <div class="row mt-3">
                          <div class="col-6">
                            <button class="btn btn-sm btn-block btn-danger" id="play" disabled>Play</button>
                          </div>
                          <div class="col-6">
                              <button class="btn btn-sm btn-block btn-info" id="download" disabled>Download</button>
                          </div>
                      </div>
                      <div class="d-none">
                          <h4>Media Stream Constraints options</h4>
                          <p>Echo cancellation: <input type="checkbox" id="echoCancellation"></p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
@endsection

@section('java_script')


    <script type="text/javascript">
        'use strict';


        let mediaRecorder;
        let recordedBlobs;

        const errorMsgElement = document.querySelector('span#errorMsg');
        const recordedVideo = document.querySelector('video#recorded');
        const recordButton = document.querySelector('button#record');
        recordButton.addEventListener('click', () => {
          if (recordButton.textContent === 'Start Recording') {
            startRecording();
          } else {
            stopRecording();
            recordButton.textContent = 'Start Recording';
            playButton.disabled = false;
            downloadButton.disabled = false;
          }
        });

        const playButton = document.querySelector('button#play');
        playButton.addEventListener('click', () => {
          const superBuffer = new Blob(recordedBlobs, {type: 'video/webm'});
          recordedVideo.src = null;
          recordedVideo.srcObject = null;
          recordedVideo.src = window.URL.createObjectURL(superBuffer);
          recordedVideo.controls = true;
          recordedVideo.play();
        });

        const downloadButton = document.querySelector('button#download');
        downloadButton.addEventListener('click', () => {
          const blob = new Blob(recordedBlobs, {type: 'video/webm'});
          const url = window.URL.createObjectURL(blob);
          const a = document.createElement('a');
          a.style.display = 'none';
          a.href = url;
          a.download = 'bangla.webm';
          document.body.appendChild(a);
          a.click();
          setTimeout(() => {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
          }, 100);
        });

        function handleDataAvailable(event) {
          console.log('handleDataAvailable', event);
          if (event.data && event.data.size > 0) {
            recordedBlobs.push(event.data);
          }
        }

        function startRecording() {
          recordedBlobs = [];
          let options = {mimeType: 'video/webm;codecs=vp9,opus'};
          if (!MediaRecorder.isTypeSupported(options.mimeType)) {
            console.error(`${options.mimeType} is not supported`);
            options = {mimeType: 'video/webm;codecs=vp8,opus'};
            if (!MediaRecorder.isTypeSupported(options.mimeType)) {
              console.error(`${options.mimeType} is not supported`);
              options = {mimeType: 'video/webm'};
              if (!MediaRecorder.isTypeSupported(options.mimeType)) {
                console.error(`${options.mimeType} is not supported`);
                options = {mimeType: ''};
              }
            }
          }

          try {
            mediaRecorder = new MediaRecorder(window.stream, options);
          } catch (e) {
            console.error('Exception while creating MediaRecorder:', e);
            errorMsgElement.innerHTML = `Exception while creating MediaRecorder: ${JSON.stringify(e)}`;
            return;
          }

          console.log('Created MediaRecorder', mediaRecorder, 'with options', options);
          recordButton.textContent = 'Stop Recording';
          playButton.disabled = true;
          downloadButton.disabled = true;
          mediaRecorder.onstop = (event) => {
            console.log('Recorder stopped: ', event);
            console.log('Recorded Blobs: ', recordedBlobs);
          };
          mediaRecorder.ondataavailable = handleDataAvailable;
          mediaRecorder.start();
          console.log('MediaRecorder started', mediaRecorder);
        }

        function stopRecording() {
          mediaRecorder.stop();
        }

        function handleSuccess(stream) {
          recordButton.disabled = false;
          console.log('getUserMedia() got stream:', stream);
          window.stream = stream;

          const gumVideo = document.querySelector('video#gum');
          gumVideo.srcObject = stream;
        }

        async function init(constraints) {
          try {
            const stream = await navigator.mediaDevices.getUserMedia(constraints);
            handleSuccess(stream);
          } catch (e) {
            console.error('navigator.getUserMedia error:', e);
            errorMsgElement.innerHTML = `navigator.getUserMedia error:${e.toString()}`;
          }
        }

        document.querySelector('button#start').addEventListener('click', async () => {
          const hasEchoCancellation = document.querySelector('#echoCancellation').checked;
          const constraints = {
            audio: {
              echoCancellation: {exact: hasEchoCancellation}
            },
            video: {
              width: 1280, height: 720
            }
          };
          console.log('Using media constraints:', constraints);
          await init(constraints);
        });

    </script>

    <script type="text/javascript">
      class State {
        constructor(startTimestamp, difference, suspended) {
          this.startTimestamp = startTimestamp;
          this.difference = difference;
          this.suspended = suspended;
        }

        static ready() {
          return new State(null, 0, 0);
        }
      }

      class Stopwatch {
        constructor(state) {
          this.state = state;
          this.requestAnimationId = null;
          this.handleClickStart = this.handleClickStart.bind(this);
          document
            .getElementById("start")
            .addEventListener("click", this.handleClickStart);
          this.handleClickStop = this.handleClickStop.bind(this);
          document
            .getElementById("stop")
            .addEventListener("click", this.handleClickStop);
          this.handleClickReset = this.handleClickReset.bind(this);
          document
            .getElementById("reset")
            .addEventListener("click", this.handleClickReset);
          this.tick = this.tick.bind(this);
          this.render();
        }

        static ready() {
          return new Stopwatch(State.ready());
        }

        setState(newState) {
          this.state = { ...this.state, ...newState };
          this.render();
        }

        tick() {
          this.setState({
            difference: new Date(new Date() - this.state.startTimestamp)
          });
          this.requestAnimationId = requestAnimationFrame(this.tick);
        }

        handleClickStart() {
          if (this.state.startTimestamp) {
            // Prevent multi clicks on start
            return;
          }
          this.setState({
            startTimestamp: new Date() - this.state.suspended,
            suspended: 0
          });
          this.requestAnimationId = requestAnimationFrame(this.tick);
        }

        handleClickStop() {
          cancelAnimationFrame(this.requestAnimationId);
          this.setState({
            startTimestamp: null,
            suspended: this.state.difference
          });
        }

        handleClickReset() {
          cancelAnimationFrame(this.requestAnimationId);
          this.setState(State.ready());
        }

        render() {
          const { difference } = this.state;
          const hundredths = (difference
            ? Math.floor(difference.getMilliseconds() / 10)
            : 0
          )
            .toString()
            .padStart(2, "0");
          const seconds = (difference ? Math.floor(difference.getSeconds()) : 0)
            .toString()
            .padStart(2, "0");
          const minutes = (difference ? Math.floor(difference.getMinutes()) : 0)
            .toString()
            .padStart(2, "0");

          // Render screen
          document.getElementById("minutes").textContent = minutes;
          document.getElementById("seconds").textContent = seconds;
          document.getElementById("hundredths").textContent = hundredths;
        }
      }

      const STOPWATCH = Stopwatch.ready();

    </script>

@endsection
