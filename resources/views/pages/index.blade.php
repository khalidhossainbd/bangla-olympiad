  @extends('layouts.main')

  @section('title', 'A center for excellence.')

  @section('content')

  {{-- <div class="container-fluid px-5 mt-3">
  	<div class="row">
  		<div class="main-img">
  			<img class="img-fluid" src="{{ asset('assets/dist/images/nbanner.jpg') }}" alt="">
  		</div>
  	</div>
  </div> --}}

  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="{{ asset('assets/dist/images/slider/slide_one.jpg') }}" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="{{ asset('assets/dist/images/slider/slide_three.jpg') }}" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="{{ asset('assets/dist/images/slider/slide_two.jpg') }}" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="container my-5">
  	<div class="row">
  		<div class="col-12 col-sm-5">
  			<img class="img-fluid" src="{{ asset('assets/dist/images/poster-bangla.jpg') }}" alt="">
  		</div>
  		<div class="col-12 col-sm-7">
  			<h3>Bangla Olympiad 2021</h3>
  			<p class="text-justify">
  				This year the Olympiad will be held online due to the pandemic situation.
          <br> <br>

          Inter-school Bangla Olympiad has been taking place in Bangladesh, for the last nine (9) years. It schedules in February, the month of International Mother Language Day, among the students of all the English Medium Schools and English Version schools. With involvements of the prominent intellectuals, artists, linguists, academicians and media personnel this event has been taking place every year. We would like to mention in here that, this event is inspired by the International Festival of Language and Culture, where in last 13 years a significant number of the Bangladeshi English Medium Students have participated, in Albania, Australia, Germany, Philippines, Romania, Kazakhstan, Kyrgyzstan, Thailand and Turkey. From the very beginning of these events and participation are being organized and sponsored by the International Hope School Bangladesh.
  			</p>
  			<a href="{{ url('/bangla_olympiad_2021') }}" class="btn btn-warning btn-lg">Upload Content</a>
  		</div>
  	</div>
  </div>

  @endsection