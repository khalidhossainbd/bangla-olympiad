@extends('layouts.main')

@section('title', 'School Deshboard')

@section('content')

<div class="bg-light">
	<div class="container mt-4">
		<div class="row">
			<div class="col">
				<h3 class="mt-5"></h3>
			</div>
			
		</div>
	</div>
</div>



  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-new-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading"> <b>School Deshboard</b>   </div>
      <hr>
      <div class="list-group list-group-flush">
        <a href="{{ url('/school-dashboard') }}" class="list-group-item list-group-item-action bg-new-light">Dashboard</a>
        <a href="{{ url('/school-profile') }}" class="list-group-item list-group-item-action bg-new-light">Profile</a>
        <a href="{{ route('students.index') }}" class="list-group-item list-group-item-action bg-new-light">Student List</a>
        <a href="{{ route('students.create') }}" class="list-group-item list-group-item-action bg-new-light">Add Student</a>
        <a href="#" class="list-group-item list-group-item-action bg-new-light">Submitted Task </a>
        <a href="{{ url('/change-password') }}" class="list-group-item list-group-item-action bg-new-light">Change Password</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></button> 
        <h3 class="pl-3">Welcome to Bangla Olympiad 2021</h3>
       {{--  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button> --}}

        {{-- <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li>
          </ul>
        </div> --}}
      </nav>

      <div class="container-fluid">
        @if ($message = Session::get('success'))
        <hr>
           <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
           </div>
           <br>
        <hr>
        @endif
        <div class="row my-3">
          <div class="col-12">
            <a class="btn btn-sm btn-info" href="{{ url('/school-profile/edit') }}">Update Profile</a>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-sm-8 p-3">
            <table class="table table-bordered">
              <tr>
                <th>School Name</th>
                <td>{{ Auth::user()->name}}</td>
              </tr>
              <tr>
                <th>School Register Email Address</th>
                <td>{{ Auth::user()->email}}</td>
              </tr>
              <tr>
                <th>School Register Mobile Number</th>
                <td>{{ Auth::user()->mobile}}</td>
              </tr>
              <tr>
                <th>School coordinator name</th>
                <td>{{ $data->coordinator == null ? 'N/A' : $data->coordinator}}</td>
              </tr>
              <tr>
                <th>School coordinator Mobile Number</th>
                <td>{{ $data->school_phone == null ? 'N/A' : $data->school_phone}}</td>
              </tr>
              <tr>
                <th>School Optional Phone Number</th>
                <td>{{ $data->mobile_number == null ? 'N/A' : $data->mobile_number}}</td>
              </tr>
              <tr>
                <th>School Address</th>
                <td>{{ $data->school_address == null ? 'N/A' : $data->school_address}}</td>
              </tr>
            </table>
          </div>
          <div class="col-12 col-sm-4">
            <h5 class="text-center">School Logo</h5>
            <img class="img-fluid d-block mx-auto" src="{{ asset('uploads/profiles/'.$data->images) }}" alt="" style="height: 200px">
          </div>
        </div>
      </div>
    </div>

  </div>




@endsection