@extends('layouts.main')

@section('title', 'School Deshboard')

@section('content')

<div class="bg-light">
	<div class="container mt-4">
		<div class="row">
			<div class="col">
				<h3 class="mt-5"></h3>
			</div>
			
		</div>
	</div>
</div>



  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-new-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading"> <b>School Deshboard</b>   </div>
      <hr>
      <div class="list-group list-group-flush">
        <a href="{{ url('/school-dashboard') }}" class="list-group-item list-group-item-action bg-new-light">Dashboard</a>
        <a href="{{ url('/school-profile') }}" class="list-group-item list-group-item-action bg-new-light">Profile</a>
        <a href="{{ route('students.index') }}" class="list-group-item list-group-item-action bg-new-light">Student List</a>
        <a href="{{ route('students.create') }}" class="list-group-item list-group-item-action bg-new-light">Add Student</a>
        <a href="#" class="list-group-item list-group-item-action bg-new-light">Submitted Task </a>
        <a href="{{ url('/change-password') }}" class="list-group-item list-group-item-action bg-new-light">Change Password</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></button> 
        <h3 class="pl-3">Welcome to Bangla Olympiad 2021</h3>
       {{--  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button> --}}

        {{-- <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li>
          </ul>
        </div> --}}
      </nav>

      <div class="container-fluid">
        <div class="row my-3">
          <div class="col-12">
            <a class="btn btn-sm btn-info" href="{{ url('/school-profile') }}">Back</a>
          </div>
        </div>
        <form action="{{ url('/school-profile/update/'.$data->id) }}" method="post" enctype="multipart/form-data">
          @csrf 
          @method('Patch') 
          <div class="row">
            <div class="col-12 col-sm-6 p-3">
                <div class="form-group">
                  <label for="name">School coordinator name</label>
                  <input type="text" name="coordinator" class="form-control" id="name" aria-describedby="name" placeholder="Enter coordinator name" value="{{ $data->coordinator }}">
                  
                </div>
                <div class="form-group">
                  <label for="school_phone">School Phone</label>
                  <input type="text" name="school_phone" class="form-control" id="school_phone" placeholder="School Phone Number" value="{{ $data->school_phone }}">
                </div>
                <div class="form-group">
                  <label for="mobile_number">School Optional Mobile</label>
                  <input type="text" name="mobile_number" class="form-control" id="mobile_number" placeholder="School Optional Mobile Number" value="{{ $data->mobile_number }}">
                </div>
                <div class="form-group">
                  <label for="school_address">School Address</label>
                  <input type="text" name="school_address" class="form-control" id="school_address" placeholder="School Address" value="{{$data->school_address}}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>            
            </div>
            <div class="col-12 col-sm-6 p-3">
              <div class="form-group">
                <label for="file">File input</label>
                <input type="file" id="files" class="form-control-file" name="images" id="file">
                <small id="file" class="form-text text-muted">Logo size 300px X 300 px</small>
              </div>
              <img class="img-fluid" id="image" src="{{ asset('uploads/profiles/'.$data->images) }}" alt="" style="height: 200px">
            </div>
          </div>
        </form>
       
      </div>
    </div>

  </div>




@endsection

@section('java_script')
<script type="text/javascript">
    document.getElementById("files").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("image").src = e.target.result;
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    };
</script>
@endsection
