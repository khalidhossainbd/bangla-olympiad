@extends('layouts.main')

@section('title', 'School Deshboard')

@section('content')

<div class="bg-light">
	<div class="container mt-4">
		<div class="row">
			<div class="col">
				<h3 class="mt-5"></h3>
			</div>
			
		</div>
	</div>
</div>



  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-new-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading"> <b>School Deshboard</b>   </div>
      <hr>
      <div class="list-group list-group-flush">
        <a href="{{ url('/school-dashboard') }}" class="list-group-item list-group-item-action bg-new-light">Dashboard</a>
        <a href="{{ url('/school-profile') }}" class="list-group-item list-group-item-action bg-new-light">Profile</a>
        <a href="{{ route('students.index') }}" class="list-group-item list-group-item-action bg-new-light">Student List</a>
        <a href="{{ route('students.create') }}" class="list-group-item list-group-item-action bg-new-light">Add Student</a>
        <a href="#" class="list-group-item list-group-item-action bg-new-light">Submitted Task </a>
        <a href="{{ url('/change-password') }}" class="list-group-item list-group-item-action bg-new-light">Change Password</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></button> 
        <h3 class="pl-3">Welcome to Bangla Olympiad 2021</h3>
       {{--  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button> --}}

        {{-- <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li>
          </ul>
        </div> --}}
      </nav>

      <div class="container-fluid">
        @if ($message = Session::get('success'))
        <hr>
           <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
           </div>
           <br>
        <hr>
        @endif
        <div class="row">
          <div class="col-12 col-sm-4">
            <img class="img-fluid p-3" src="{{ asset('assets/dist/images/banner_new.jpg') }}" alt="Event Banner">
          </div>
          <div class="col-12 col-sm-8">
            <p class="font-qs text-justify p-4">
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Cum quia, tempore fugiat quis provident tempora dolore corrupti repudiandae, delectus magnam tenetur alias repellendus error sequi, obcaecati molestias fugit debitis ipsum?
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laborum aliquid soluta modi rem ab praesentium pariatur, explicabo vero saepe voluptatum neque laboriosam cum expedita dolores sequi debitis sed accusantium hic.

              <br> <br>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis, atque rem officia in fugiat ipsum totam minima, molestiae exercitationem consectetur unde facilis nisi, dolore natus quis tempore laudantium vitae sint?
              Lorem ipsum dolor sit, amet consectetur adipisicing, elit. Assumenda laudantium excepturi autem, aperiam sequi doloremque magnam incidunt numquam molestiae cum praesentium laborum corrupti qui saepe dignissimos asperiores modi ea sed?  
            </p>
          </div>
        </div>
      </div>
    </div>

  </div>




@endsection