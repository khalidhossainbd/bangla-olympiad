@extends('layouts.main')
@section('title', 'School Deshboard')
@section('content')
<div class="bg-light">
  <div class="container mt-4">
    <div class="row">
      <div class="col">
        <h3 class="mt-5"></h3>
      </div>
      
    </div>
  </div>
</div>
<div class="d-flex" id="wrapper">
  <!-- Sidebar -->
  <div class="bg-new-light border-right" id="sidebar-wrapper">
    <div class="sidebar-heading"> <b>School Deshboard</b>   </div>
    <hr>
    <div class="list-group list-group-flush">
      <a href="{{ url('/school-dashboard') }}" class="list-group-item list-group-item-action bg-new-light">Dashboard</a>
      <a href="{{ url('/school-profile') }}" class="list-group-item list-group-item-action bg-new-light">Profile</a>
      <a href="{{ route('students.index') }}" class="list-group-item list-group-item-action bg-new-light">Student List</a>
      <a href="{{ route('students.create') }}" class="list-group-item list-group-item-action bg-new-light">Add Student</a>
      <a href="#" class="list-group-item list-group-item-action bg-new-light">Submitted Task </a>
      <a href="{{ url('/change-password') }}" class="list-group-item list-group-item-action bg-new-light">Change Password</a>
    </div>
  </div>
  <!-- /#sidebar-wrapper -->
  <!-- Page Content -->
  <div id="page-content-wrapper">
    <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
      <button class="btn btn-primary" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></button>
      <h3 class="pl-3">Welcome to Bangla Olympiad 2021</h3>
      {{--  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button> --}}
      {{-- <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Dropdown
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
        </ul>
      </div> --}}
    </nav>
    <div class="container-fluid">
      @if ($message = Session::get('success'))
        <hr>
           <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
           </div>
           <br>
        <hr>
        @endif
      <div class="row my-3">
        <div class="col-12">
          <a class="btn btn-sm btn-info" href="{{ url('/school-profile') }}">Back</a>
        </div>
      </div>
      <form action="{{ url('/change-password/'.Auth::user()->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('Patch')
        <div class="row">
          <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="">User Email</label>
            <input type="email" class="form-control" disabled value="{{ Auth::user()->email }}" >
          </div>
          <div class="form-group">
            <label for="old_password">Old Password</label>
            <input type="password" class="form-control @error('old_password') is-invalid @enderror" name="old_password" required autocomplete="old_password">
            @error('old_password')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="password">New Password</label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
            @error('password')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="password-confirm" class=" col-form-label text-md-right">{{ __('Confirm Password') }}</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
          </div>
          <button type="submit" class="btn btn-primary mb-2">Update Password</button>
          </div>
        </div>
      </form>
      
    </div>
  </div>
</div>
@endsection
@section('java_script')
<script type="text/javascript">
document.getElementById("files").onchange = function () {
var reader = new FileReader();
reader.onload = function (e) {
// get loaded data and render thumbnail.
document.getElementById("image").src = e.target.result;
};
// read the image file as a data URL.
reader.readAsDataURL(this.files[0]);
};
</script>
@endsection