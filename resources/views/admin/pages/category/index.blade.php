@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">
        <h3 class="my-4">Catagory List</h3>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"></li>
        </ol>
        <div class="row">
            <div class="col-12">
                <table class="table table-bordered table-hover" id="dataTable">
                    <thead>
                        <tr>
                            <th>Serial No.</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($category)>0)
                        @foreach($category as $item)
                        <tr>
                            <td>{{ $loop->iteration  }}</td>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->description }}</td>
                            <td {{ $item->status=='0' ? 'style=color:red' : ''}}> {{ $item->status=='1' ? 'Active' : 'Inactive'}}</td>
                            <td>
                                <a href="{{ url('/admin/category/' . $item->id . '/edit') }}" title="Edit Event"><button class="btn btn-primary btn-xs">Edit</button></a>
                                @php if($item->status >0){ @endphp
                                    <form method="POST" action="{{ url('/admin/category/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                        {{ method_field('PATCH') }}
                                        {{ csrf_field() }}
                                        <input style="display: none;" name="status" value="0"/>
                                        <button type="submit" class="btn btn-danger btn-xs" title="Delete Event" onclick="return confirm(&quot;Confirm Inactive?&quot;)"> Inactive</button>
                                    </form>
                                @php }else{ @endphp
                                    <form method="POST" action="{{ url('/admin/category/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                        {{ method_field('PATCH') }}
                                        {{ csrf_field() }}
                                        <input style="display: none;" name="status" value="1"/>
                                        <button type="submit" class="btn btn-success btn-xs" title="Delete Event" onclick="return confirm(&quot;Confirm Active?&quot;)">Active</button>
                                    </form>
                                @php    }
                                @endphp
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5">No Data Found</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>                

@endsection

