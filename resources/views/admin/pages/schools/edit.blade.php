@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">
        <h3 class="mt-4">Edit School Info</h3>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"></li>
        </ol>
        <form method="POST" action="{{ url('/admin/school/'.$school->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
        <div class="row">
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label for="name">School Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $school->name }}" required >
                </div>
                <div class="form-group">
                    <label for="branch_name">School Baranch Name</label>
                    <input type="text" class="form-control" id="branch_name" name="branch_name" value="{{ $school->branch_name }}" required >
                </div>
                <div class="form-group">
                    <label for="email">School Email</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{ $school->email }}" required >
                </div>
                <div class="form-group">
                    <label for="">Status</label>
                    <select class="form-control @error('status') is-invalid @enderror" name="status" required="required">
                      <option value="" active>Select a option</option>
                      <option {{ $school->status == 'Active' ? 'selected' : '' }}>Active</option>
                      <option {{ $school->status == 'Inactive' ? 'selected' : '' }}>Inactive</option>
                    </select>
                    @error('status')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div> 
                <button type="submit" class="btn btn-success">Update</button>
            </div>
            <div class="col-12 col-sm-6">
                
            </div>
        </div>
        </form>
    </div>                

@endsection

