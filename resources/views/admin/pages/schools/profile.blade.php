@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">
        <h1 class="mt-4">School Profile</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"></li>
        </ol>
        
            <div class="row">
                <div class="col-12 col-sm-8">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th width="40%">School Name</th>
                            <td>{{ $profile->name  }}</td>
                        </tr>
                        <tr>
                            <th width="40%">School Branch Name</th>
                            <td>{{ $profile->branch_name  }}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{ $profile->email }}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>{{ $profile->status }}</td>
                        </tr>
                        <tr>
                            <th>School Phone Number</th>
                            <td>{{ $profile->email }}</td>
                        </tr>
                        <tr>
                            <th>Coordinator Name</th>
                            <td>{{ $profile->schoolprofile->coordinator }}</td>
                        </tr>
                        <tr>
                            <th>Coordinator Mobile Number</th>
                            <td>{{ $profile->schoolprofile->mobile_number }}</td>
                        </tr>
                        <tr>
                            <th>School Phone Number</th>
                            <td>{{ $profile->schoolprofile->school_phone }}</td>
                        </tr>
                        <tr>
                            <th>School Address</th>
                            <td>{{ $profile->schoolprofile->school_address }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-12 col-sm-4">
                    <h4 class="text-center">School Logo</h4>
                    <img class="img-fluid d-block mx-auto" src="{{ asset('uploads/profiles/'.$profile->schoolprofile->images) }}" alt="" style="max-height: 200px">
                </div>
            </div>
        
        
        
    </div>                

@endsection

