@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">
        <h3 class="mt-4">Students List</h3>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"></li>
        </ol>
        <div class="row">
            <div class="col-12">
                <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Sl No.</th>
                            <th>School Name</th>
                            <th>School Branch name</th>
                            <th>Student Name</th>
                            <th>Student Code</th>
                            <th>Class </th>
                            <th>Mobile Number</th>
                            <th>Category Title</th>
                            <th>Group Name</th>
                            <th>Status</th>
                            {{-- <th>Action</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($students)>0)
                        @foreach($students as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->user->name }}</td>
                            <td>{{ $item->user->branch_name }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->code }}</td>
                            <td>{{ $item->class }}</td>
                            <td>{{ $item->mobile }}</td>
                            <td>{{ $item->category->title }}</td>
                            <td>{{ $item->group->title }}</td>
                            <td>{{ $item->status == 1 ? 'Active' : '' }}</td>
                           {{--  <td>
                                <a class="btn btn-sm btn-success" href="{{ url('/admin/school/'.$item->id.'/edit') }}"><i class="fas fa-edit"></i></a>
                                <a class="btn btn-sm btn-primary" href="{{ url('/admin/school/profile/'.$item->id) }}">View Profile</a>
                            </td> --}}
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="10" class="text-center">No Data Found</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>                

@endsection

