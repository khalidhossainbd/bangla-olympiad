@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">
        <h3 class="my-4">Group List</h3>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"></li>
        </ol>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Serial No.</th>
                                <th>Category Title</th>
                                <th>Group Name</th>
                                <th>Student Number</th>
                                <th>Clases</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($group as $item)
                                <tr>
                                    <td>{{ $loop->iteration  }}</td>
                                    <td>{{ $item->Category->title }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->student_amount }}</td>
                                    <td>{{ $item->class_name }}</td>
                                    <td>{{ $item->description }}</td>
                                    
                                    <td>
                                        @php 
                                            if($item->status >0){
                                                echo "Active";
                                            }else{
                                                echo"Inactive";
                                            }
                                        @endphp
                                    </td>
                                    <td>
                                        <a href="{{ url('/admin/groups/' . $item->id . '/edit') }}" title="Edit Event"><button class="btn btn-primary btn-xs">Edit</button></a>
                                        @php if($item->status >0){ @endphp
                                                <form method="POST" action="{{ url('/admin/groups/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                    {{ method_field('PATCH') }}
                                                    {{ csrf_field() }}
                                                    <input style="display: none;" name="status" value="0"/>
                                                    <button type="submit" class="btn btn-danger btn-xs" title="Delete Event" onclick="return confirm(&quot;Confirm Inactive?&quot;)">Inactive</button>
                                                </form>
                                        @php }else{ @endphp
                                                <form method="POST" action="{{ url('/admin/groups/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                    {{ method_field('PATCH') }}
                                                    {{ csrf_field() }}
                                                    <input style="display: none;" name="status" value="1"/>
                                                    <button type="submit" class="btn btn-success btn-xs" title="Delete Event" onclick="return confirm(&quot;Confirm Active?&quot;)"> Active</button>
                                                </form>
                                        @php    }
                                        @endphp
                                        {{-- <form method="POST" action="{{ url('/admin/groups' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-xs" title="Delete Event" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                        </form> --}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $group->render() !!}
                </div>
            </div>
        </div>
    </div>                

@endsection
