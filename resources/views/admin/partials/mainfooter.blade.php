<footer class="py-4 bg-light mt-auto">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="text-muted">Copyright &copy; Bangla Olympiad 2020</div>
            
            </div>
            <div class="col-md-6">
                <p class="text-right">Developed by:
                &middot;
                <a href="#">Md KHalid Hossain</a></p>
            </div>
        </div>
        {{-- <div class="d-flex align-items-center justify-content-between small">
            <div class="text-muted">Copyright &copy; www.krishanbari.xyz 2020</div>
            <div>
                Developed by:
                &middot;
                <a href="#">Md KHalid Hossain</a>
            </div>
        </div> --}}
    </div>
</footer>