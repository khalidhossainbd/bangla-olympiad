@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">

        <h2 class="mt-4" style="text-decoration: underline;">Teacher Profile</h2>
        
            @if ($message = Session::get('success'))
            <hr>
               <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
               </div>
               <br>
            <hr>
            @endif
        
        

        <div class="card mb-4">
            {{-- <div class="card-header"><i class="fas fa-table mr-1"></i>DataTable Example</div> --}}
            <div class="card-body">
              <p><b>Name: {{ $teacher->name }}</b></p>
              <p class="m-0">Eamil: {{ $teacher->email }}</p>
              <p class="m-0">Mobile: {{ $teacher->mobile }}</p>
              <p class="m-0">Designation: {{ $teacher->designation }}</p>
              <p class="mt-0">Joining Date: {{ $teacher->join_date }}</p>

              <a class="btn btn-sm btn-info mb-3" href="{{ url('admin/teacher/profile/edit/'.$pro_data->id) }}">Update</a>
              <div class="row">
                <div class="col-8">
                  <div class="table-responsive">
                      <table class="table table-hover" width="100%" cellspacing="0">
                          <tr>
                            <th width="200px">Father's Name</th>
                            <td>{{ $pro_data->father_name == null ? 'N/A' : $pro_data->father_name }}</td>
                          </tr>                        
                          <tr>
                            <th>Mother's Name</th>
                            <td>{{ $pro_data->mother_name == null ? 'N/A' : $pro_data->mother_name }}</td>
                          </tr>
                          <tr>
                            <th>Date of Birth</th>
                            <td>{{ $pro_data->date_of_birth == null ? 'N/A' : $pro_data->date_of_birth }}</td>
                          </tr>
                          <tr>
                            <th>Gender</th>
                            <td>{{ $pro_data->gender == null ? 'N/A' : $pro_data->gender }}</td>
                          </tr>
                          <tr>
                            <th>Year of Experience</th>
                            <td>{{ $pro_data->year_experience == null ? 'N/A' : $pro_data->year_experience }}</td>
                          </tr>
                            
                            <tr>
                              <th>Home Phone</th>
                              <td>{{ $pro_data->home_phone == null ? 'N/A' : $pro_data->home_phone }}</td>
                            </tr>

                            <tr>
                              <th>Present Address</th>
                              <td>{{ $pro_data->preaent_address == null ? 'N/A' : $pro_data->preaent_address }}</td>
                            </tr>
                            <tr>
                              <th>Parment Address</th>
                              <td>{{ $pro_data->parmanent_address == null ? 'N/A' : $pro_data->parmanent_address }}</td>
                            </tr>
                            
                            <tr>
                              <th>Quelification</th>
                              <td>{!! $pro_data->content !!}</td>
                            </tr>
                          </tr>
                      </table>
                  </div>
                </div>
                <div class="col-4">
                  <div class="mb-3">
                    <img class="d-block mr-auto ml-auto" src="{{ asset('uploads/profiles/'.$pro_data->images) }}" alt="Profile Images">
                  </div>
                  <div>
                    <a class="btn btn-sm btn-block btn-warning" href="#">Download CV</a>
                  </div>
                </div>
              </div>
                
            </div>
        </div>
        
    </div>                

@endsection

