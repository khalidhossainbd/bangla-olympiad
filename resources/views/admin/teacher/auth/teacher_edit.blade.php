@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">
        <h3 class="mt-4">Edit Examiner Info</h3>
        <hr>
        
            {{-- <li class="breadcrumb-item active">Dashboard</li> --}}
            @if ($message = Session::get('success'))
	           <div class="alert alert-success alert-block">
	              <button type="button" class="close" data-dismiss="alert">×</button>
	              <strong>{{ $message }}</strong>
	           </div>
	           <br>
	        @endif
		
        <div class="row">
        	<div class="col-md-6">    
        		<form action="{{ url('admin/teacher/'.$data->id) }}" method="post">
        		@csrf   
                @method('Patch')       	
        		<div class="form-group">
        		    <label for="">Teacher Full Name</label>
        		    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $data->name }}"  autofocus autocomplete="name" required> 
        		    @error('name')
        		        <span class="invalid-feedback" role="alert">
        		            <strong>{{ $message }}</strong>
        		        </span>
        		    @enderror
        		</div>

                <div class="form-group">
                    <label for="">Designation</label>
                    <input type="text" class="form-control @error('designation') is-invalid @enderror" name="designation" value="{{ $data->designation }}" autofocus autocomplete="designation" required> 
                    @error('designation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div> 

        		<div class="form-group">
        		    <label for="">User Email / Login Email</label>
        		    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $data->email }}"  autofocus autocomplete="email" required> 
        		    @error('email')
        		        <span class="invalid-feedback" role="alert">
        		            <strong>{{ $message }}</strong>
        		        </span>
        		    @enderror
        		</div>
                
        		<div class="form-group">
        		    <label for="">Mobile Number</label>
        		    <input type="text" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ $data->mobile }}" autofocus autocomplete="mobile" required> 
        		    @error('mobile')
        		        <span class="invalid-feedback" role="alert">
        		            <strong>{{ $message }}</strong>
        		        </span>
        		    @enderror
        		</div> 
                <div class="form-group">
                    <label for="password">Password</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" value="{{ $data->password }}" required autocomplete="new-password" disabled>

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Status</label>
                    <select class="form-control @error('status') is-invalid @enderror" name="status" required="required">
                      <option value="" active>Select a option</option>
                      <option {{ $data->status == 'Active' ? 'selected' : '' }}>Active</option>
                      <option {{ $data->status == 'Inactive' ? 'selected' : '' }}>Inactive</option>
                    </select>
                    @error('type')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div> 
        	</div>
        	<div class="col-md-6">
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-6">
        		<button type="submit" class="btn btn-primary mb-2">Update</button>
        		{{-- <button type="reset" class="btn btn-primary mb-2">Reset</button> --}}
        	</div>
        </div>
        </form>
    </div>                

@endsection

