@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">

        <h3 class="mt-4" style="text-decoration: underline;">Examiners List</h3>
        
            @if ($message = Session::get('success'))
            <hr>
               <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
               </div>
               <br>
            <hr>
            @endif
        
        

        <div class="card mb-4">
            {{-- <div class="card-header"><i class="fas fa-table mr-1"></i>DataTable Example</div> --}}
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                              <th scope="col">Sl No</th>
                              <th scope="col">Full Name</th>
                              <th scope="col">Designation</th>
                              <th scope="col">Email</th>
                              <th scope="col">Mobile</th>
                              <th scope="col">Status</th>
                              <th scope="col">Action</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($data as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->designation }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->mobile }}</td>
                                    <td {{ $item->status=='Inactive' ? 'style=color:red' : ''}}>{{ $item->status }}</td>
                                    <td>
                                        <a class="btn btn-sm btn-primary" href="{{ url('admin/teacher/'.$item->id.'/edit') }}"><i class="fas fa-edit"></i></a> 

                                        {{-- <a class="btn btn-sm btn-info" href="{{ url('admin/teacher/profile/'.$item->id) }}">View Profile</a> --}}                                       
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>                

@endsection

