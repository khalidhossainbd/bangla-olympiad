@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">
        <h3 class="mt-4">Edit Teacher Profile</h3>
        <hr>
        
            {{-- <li class="breadcrumb-item active">Dashboard</li> --}}
            @if ($message = Session::get('success'))
	           <div class="alert alert-success alert-block">
	              <button type="button" class="close" data-dismiss="alert">×</button>
	              <strong>{{ $message }}</strong>
	           </div>
	           <br>
	        @endif
		
        <div class="row">
        	<div class="col-md-6">    
        		<form action="{{ url('admin/teacher/profile/update/'.$pro_data->id) }}" method="post" enctype="multipart/form-data">
        		@csrf 
                @method('Patch')       	
        		<div class="form-group">
        		    <label for="">Father's Name</label>
        		    <input type="text" class="form-control @error('father_name') is-invalid @enderror" name="father_name" value="{{ $pro_data->father_name }}" placeholder="Father's Name" autofocus autocomplete="father_name" required> 
        		    @error('father_name')
        		        <span class="invalid-feedback" role="alert">
        		            <strong>{{ $message }}</strong>
        		        </span>
        		    @enderror
        		</div>
                <div class="form-group">
                    <label for="">Mother's Name</label>
                    <input type="text" class="form-control @error('mother_name') is-invalid @enderror" name="mother_name" value="{{ $pro_data->mother_name }}" placeholder="Mother's Name" autofocus autocomplete="mother_name" required> 
                    @error('mother_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Date of Birth</label>
                    <input type="date" class="form-control @error('date_of_birth') is-invalid @enderror" name="date_of_birth" value="{{ $pro_data->date_of_birth }}" placeholder="Date of Birth" autofocus autocomplete="date_of_birth" required> 
                    @error('date_of_birth')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div> 
                <div class="form-group">
                    <label for="">Gender</label>
                    <select class="form-control @error('gender') is-invalid @enderror" name="gender" required="required">
                      <option value="" active>Select a option</option>
                      <option {{ $pro_data->gender == 'Male' ? 'selected' : '' }}>Male</option>
                      <option {{ $pro_data->gender == 'Female' ? 'selected' : '' }}>Female</option>
                      <option {{ $pro_data->gender == 'Others' ? 'selected' : '' }}>Others</option>
                    </select>
                    @error('gender')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Year of Experience</label>
                    <input type="text" class="form-control @error('year_experience') is-invalid @enderror" name="year_experience" value="{{ $pro_data->year_experience }}" placeholder="Year of Experience" autofocus autocomplete="year_experience" required> 
                    @error('year_experience')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div> 
        		<div class="form-group">
        		    <label for="">Home Phone</label>
        		    <input type="text" class="form-control @error('home_phone') is-invalid @enderror" name="home_phone" value="{{ $pro_data->home_phone }}" placeholder="Mobile Number" autofocus autocomplete="home_phone" required> 
        		    @error('home_phone')
        		        <span class="invalid-feedback" role="alert">
        		            <strong>{{ $message }}</strong>
        		        </span>
        		    @enderror
        		</div>
                <div class="form-group">
                    <label for="content">Present Address</label>
                    <textarea class="form-control" rows="3" name="preaent_address">{{ $pro_data->preaent_address }}</textarea>
                </div>
                <div class="form-group">
                    <label for="content">Permanent Address</label>
                    <textarea class="form-control" rows="3" name="parmanent_address">{{ $pro_data->parmanent_address }}</textarea>
                </div>
        	</div>
        	<div class="col-md-6">
                <div class="form-group">
                    <label for="content">Qualification</label>
                    <textarea id="editor" class="form-control" rows="5" name="content">{!! $pro_data->content !!}</textarea>
                </div> 
                <div class="form-group">
                    <label for="files">File input</label><br>
                    <input type="file" id="files" name="images">
                    <p class="help-block">Image size must be 300 x 300 PX</p>
                </div>
                <div class="form-group">
                    <label>Selected Image</label><br>
                    <img style="max-height: 300px;" class="img-responsive" id="image" src="{{ asset('uploads/profiles/'.$pro_data->images) }}" />
                </div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-6">
        		<button type="submit" class="btn btn-primary mb-2">Update</button>
        		
        	</div>
        </div>
        </form>
    </div>                

@endsection

