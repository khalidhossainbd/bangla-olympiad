<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Language" content="en">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Bangla Olympiad | School Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
        <meta name="description" content="">
        <meta name="msapplication-tap-highlight" content="no">
        <link rel="icon" href="{{ asset('assets/dist/images/logo.png') }}" type="image/gif" sizes="16x16">
        <link href="{{ asset('main/main.css') }}" rel="stylesheet">

        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/fontawesome.css" integrity="sha384-eHoocPgXsiuZh+Yy6+7DsKAerLXyJmu2Hadh4QYyt+8v86geixVYwFqUvMU8X90l" crossorigin="anonymous"/>
    </head>
    <body>

    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
                
        @include('schools.partials.navber')  
        <div class="app-main">
            @include('schools.partials.sidebar')
            <div class="app-main__outer">
        
            @yield('content')

            @include('schools.partials.footer')

            </div>
        </div>
    </div>

        <script type="text/javascript" src="{{ asset('main/assets/scripts/main.js') }}"></script>
    </body>
</html>