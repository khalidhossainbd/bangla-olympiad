<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="MD Khalid Hossain">
	<meta property="og:url"           content="" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="" />
	<meta property="og:description"   content="" />
	<meta property="og:image" content="" />

	<link rel="icon" href="{{ asset('assets/dist/images/logo.png') }}" type="image/gif" sizes="16x16">
	<title>Bangla Olympiad | @yield('title')</title>

	<link rel="stylesheet" href="{{ asset('assets/node_modules/bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/animate.css/animate.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/dist/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/dist/css/style.css') }}">
</head>
<body>

	@include('partials.navber')

	@yield('content')

	@include('partials.footer')
	
	
	<script type="text/javascript" src="{{ asset('assets/node_modules/mdbootstrap/js/jquery.min.js') }}"></script>
	     
	<script type="text/javascript" src="{{ asset('assets/node_modules/mdbootstrap/js/popper.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/node_modules/mdbootstrap/js/bootstrap.min.js') }}"></script>

	  <script>
	    $("#menu-toggle").click(function(e) {
	      e.preventDefault();
	      $("#wrapper").toggleClass("toggled");
	    });
	  </script>


	 @yield('java_script')

</body>
</html>